package com.example.threeexpandablelistviewdome.adapter;


import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

import com.example.threeexpandablelistviewdome.MyListView;
import com.example.threeexpandablelistviewdome.R;
import com.example.threeexpandablelistviewdome.model.ChildTreenodeInfo;
import com.example.threeexpandablelistviewdome.model.ParentTreenodeInfo;
import com.example.threeexpandablelistviewdome.model.TreenodeInfo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.sax.StartElementListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;


public class TreeViewAdapter extends BaseExpandableListAdapter {

	private Context context;
	private List<TreenodeInfo> groupNodes;
	private OnClickListener onMyClickListener;

	public TreeViewAdapter(Context view, List<TreenodeInfo> groupNodes) {
		this.context = view;
		this.groupNodes = groupNodes;
	}

	public void setMyOnclickListener(OnClickListener listener) {
		onMyClickListener = listener;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return groupNodes.get(groupPosition).childNodes.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		DisplayMetrics dm = new DisplayMetrics();
		Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
		display.getMetrics(dm);

		ParentTreenodeInfo parentnode = groupNodes.get(groupPosition).childNodes
				.get(childPosition);
		List<ChildTreenodeInfo> childnodes = parentnode.childNodes;

		View view = LayoutInflater.from(context).inflate(
				R.layout.listitem_layout, null);
		RelativeLayout l_contant = (RelativeLayout) view
				.findViewById(R.id.l_contant);
		l_contant.setTag(groupPosition + "," + childPosition);
		l_contant.setOnClickListener(onMyClickListener);
		if (groupPosition == groupNodes.size() - 1
				&& childPosition == groupNodes.get(groupPosition).childNodes
						.size() - 1) {
			l_contant.setPadding(0, 0, 0, 25 * ((int) dm.density));
		}
		TextView txtname = (TextView) view.findViewById(R.id.txtItemName);
		txtname.setText(parentnode.nodeName);
		ImageView img_jiantou = (ImageView) view.findViewById(R.id.img_jiantou);

		FrameLayout f_list = (FrameLayout) view.findViewById(R.id.f_listview);
		final MyListView mylist = (MyListView) view.findViewById(R.id.lv_items);

		TreenodeListItemAdapter listAdapter = new TreenodeListItemAdapter(
				context, childnodes);

		mylist.setAdapter(listAdapter);
		if (childnodes != null && childnodes.size() > 0) {

			mylist.width = dm.widthPixels;
			mylist.height = (int) (((childnodes.size()) * 50) * dm.scaledDensity);
			mylist.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
//					context.startActivity(new Intent(context, IntroduceActivity.class));
				}
			});
			if (parentnode.isSelected) {
				f_list.setVisibility(View.VISIBLE);
				img_jiantou.setImageResource(R.drawable.img_jiantou_up);
			} else {
				f_list.setVisibility(View.GONE);
				img_jiantou.setImageResource(R.drawable.img_jiantou_down);
			}
		}

		return view;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return groupNodes.get(groupPosition).childNodes.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return groupNodes.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return groupNodes.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		View view = LayoutInflater.from(context).inflate(
				R.layout.expandableparentgroup_layout, null);

		TreenodeInfo theNode = (TreenodeInfo) getGroup(groupPosition);

		TextView tv = (TextView) view.findViewById(R.id.txtItemName);
		tv.setText(theNode.nodeName);
		return view;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public ExpandableListView getNewExpandableListView() {
		AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT, 50);
		ExpandableListView superTreeView = new ExpandableListView(context);
		superTreeView.setLayoutParams(lp);
		return superTreeView;
	}
	
	
	

}

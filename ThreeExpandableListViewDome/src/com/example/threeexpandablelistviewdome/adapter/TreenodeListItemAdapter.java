package com.example.threeexpandablelistviewdome.adapter;

import java.util.ArrayList;
import java.util.List;

import com.example.threeexpandablelistviewdome.R;
import com.example.threeexpandablelistviewdome.model.ChildTreenodeInfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class TreenodeListItemAdapter extends BaseAdapter {
	private Context context;
	private List<ChildTreenodeInfo> nodes;

	public TreenodeListItemAdapter(Context context,
			List<ChildTreenodeInfo> childNodes) {
		this.context = context;
		if (childNodes == null)
			nodes = new ArrayList<ChildTreenodeInfo>();
		else
			nodes = childNodes;
	}

	@Override
	public int getCount() {
		return nodes.size();
	}

	@Override
	public Object getItem(int position) {
		return nodes.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 实例化布局文件
		View view = LayoutInflater.from(context).inflate(
				R.layout.expandableitem_layout, null);
		ChildTreenodeInfo theNode = nodes.get(position);

		TextView tv = (TextView) view.findViewById(R.id.txtItemName);
		tv.setText(theNode.nodeName);
		return view;
	}


}

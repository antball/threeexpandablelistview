package com.example.threeexpandablelistviewdome.model;

import java.util.List;

/**
 * 
 * @author 200179
 * @version create time��2013-5-2 ����2:11:19 
 */
public class ParentTreenodeInfo {
	public String nodeId;
	public String nodeName;
	public boolean isSelected;
	public List<ChildTreenodeInfo> childNodes;
}

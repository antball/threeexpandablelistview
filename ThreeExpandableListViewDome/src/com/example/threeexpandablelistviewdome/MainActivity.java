package com.example.threeexpandablelistviewdome;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import com.example.threeexpandablelistviewdome.adapter.TreeViewAdapter;
import com.example.threeexpandablelistviewdome.model.ChildTreenodeInfo;
import com.example.threeexpandablelistviewdome.model.ParentTreenodeInfo;
import com.example.threeexpandablelistviewdome.model.TreenodeInfo;

import android.R.integer;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.TextView;

/**
 * 文化列表界面
 * 
 * @author 200179
 * @version create time：2013-4-27 下午9:47:44
 */
public class MainActivity extends Activity {

	ExpandableListView exlv;
	List<TreenodeInfo> groupNodes = new ArrayList<TreenodeInfo>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initPages();

	}

	public void initPages() {
		TextView txtTitle =(TextView) findViewById(R.id.txtTitle);
		txtTitle.setText("测试");
		
		getItems();
		exlv = (ExpandableListView) findViewById(R.id.exlv);

		final TreeViewAdapter adapter = new TreeViewAdapter(this, groupNodes);
		adapter.setMyOnclickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String curAddress = v.getTag().toString();
				String[] tempStr = curAddress.split(",");
				
				ParentTreenodeInfo parentnode = groupNodes.get(Integer
						.parseInt(tempStr[0])).childNodes.get(Integer
						.parseInt(tempStr[1]));
				parentnode.nodeName = "haha";
				parentnode.isSelected = !parentnode.isSelected;
				parentnode.childNodes = new ArrayList<ChildTreenodeInfo>();
				ChildTreenodeInfo childnode = new ChildTreenodeInfo();
				childnode.nodeName = "child1";
				parentnode.childNodes.add(childnode);

				childnode = new ChildTreenodeInfo();
				childnode.nodeName = "child2";
				parentnode.childNodes.add(childnode);
				childnode = new ChildTreenodeInfo();
				childnode.nodeName = "child3";
				parentnode.childNodes.add(childnode);

				adapter.notifyDataSetChanged();
				
			}
		});

		exlv.setAdapter(adapter);
		exlv.setGroupIndicator(null);
		exlv.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				return true;
			}
		});

		for (int i = 0; i < adapter.getGroupCount(); i++) {
			exlv.expandGroup(i);
		}

	}


	private void getItems() {

		
		List<ParentTreenodeInfo> parentNodes = new ArrayList<ParentTreenodeInfo>();
		
		TreenodeInfo groupNode = new TreenodeInfo();
		ParentTreenodeInfo parentNode = new ParentTreenodeInfo();
		groupNode.nodeName = "父标题一";
		parentNode.nodeId = "1";
		parentNode.nodeName = "test1";
		parentNode.isSelected = false;
		parentNodes.add(parentNode);
		ParentTreenodeInfo parentNode2 = new ParentTreenodeInfo();
		parentNode2.nodeId = "2";
		parentNode2.nodeName = "test2";
		parentNode2.isSelected = false;
		parentNodes.add(parentNode2);
		groupNode.childNodes = parentNodes;
		groupNodes.add(groupNode);
		
		List<ParentTreenodeInfo> parentNodes2 = new ArrayList<ParentTreenodeInfo>();

		TreenodeInfo groupNode2 = new TreenodeInfo();
		groupNode2.nodeName = "父标题二";
		ParentTreenodeInfo parentNode3 = new ParentTreenodeInfo();
		parentNode3.nodeId = "1";
		parentNode3.nodeName = "小样1";
		parentNode3.isSelected = false;
		parentNodes2.add(parentNode3);
		ParentTreenodeInfo parentNode4 = new ParentTreenodeInfo();
		parentNode4.nodeId = "2";
		parentNode4.nodeName = "小样2";
		parentNode4.isSelected = false;
		parentNodes2.add(parentNode4);
		groupNode2.childNodes = parentNodes2;
		
		groupNodes.add(groupNode2);
	}

}

package com.example.threeexpandablelistviewdome;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 
 * @author 200179
 * @version create time��2013-5-1 ����8:48:31 
 */
public class MyListView  extends ListView {
	
	public int height;
	public int width = 320;

	
	public MyListView(Context context){
		super(context);
	}

	public MyListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension((int) (width), (int) (height));
	}
}
